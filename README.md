# Goals

This project provides docker compose file to deploy photo-uploader-service

https://gitlab.com/XavierBouvard/resize-photo  
https://gitlab.com/XavierBouvard/upload-photo  

# Launch 

Run:  
```sh
docker-compose pull  
docker-compose build  
docker-compose down # ensures no previous version is running  
docker-compose up -d  
```

go to http://localhost:8888
